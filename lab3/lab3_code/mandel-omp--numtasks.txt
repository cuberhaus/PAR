1

Computation of the Mandelbrot set with:
    center = (0, 0) 
    size = 2
    maximum iterations = 10000

Total execution time (in seconds): 2.863394

Mandelbrot set: Computed
Histogram for Mandelbrot set: Computed
2

Computation of the Mandelbrot set with:
    center = (0, 0) 
    size = 2
    maximum iterations = 10000

Total execution time (in seconds): 1.454987

Mandelbrot set: Computed
Histogram for Mandelbrot set: Computed
4

Computation of the Mandelbrot set with:
    center = (0, 0) 
    size = 2
    maximum iterations = 10000

Total execution time (in seconds): 1.443321

Mandelbrot set: Computed
Histogram for Mandelbrot set: Computed
8

Computation of the Mandelbrot set with:
    center = (0, 0) 
    size = 2
    maximum iterations = 10000

Total execution time (in seconds): 1.198714

Mandelbrot set: Computed
Histogram for Mandelbrot set: Computed
16

Computation of the Mandelbrot set with:
    center = (0, 0) 
    size = 2
    maximum iterations = 10000

Total execution time (in seconds): 0.730999

Mandelbrot set: Computed
Histogram for Mandelbrot set: Computed
32

Computation of the Mandelbrot set with:
    center = (0, 0) 
    size = 2
    maximum iterations = 10000

Total execution time (in seconds): 0.411163

Mandelbrot set: Computed
Histogram for Mandelbrot set: Computed
64

Computation of the Mandelbrot set with:
    center = (0, 0) 
    size = 2
    maximum iterations = 10000

Total execution time (in seconds): 0.224216

Mandelbrot set: Computed
Histogram for Mandelbrot set: Computed
128

Computation of the Mandelbrot set with:
    center = (0, 0) 
    size = 2
    maximum iterations = 10000

Total execution time (in seconds): 0.108194

Mandelbrot set: Computed
Histogram for Mandelbrot set: Computed
256

Computation of the Mandelbrot set with:
    center = (0, 0) 
    size = 2
    maximum iterations = 10000

Total execution time (in seconds): 0.089570

Mandelbrot set: Computed
Histogram for Mandelbrot set: Computed
512

Computation of the Mandelbrot set with:
    center = (0, 0) 
    size = 2
    maximum iterations = 10000

Total execution time (in seconds): 0.084505

Mandelbrot set: Computed
Histogram for Mandelbrot set: Computed
1024

Computation of the Mandelbrot set with:
    center = (0, 0) 
    size = 2
    maximum iterations = 10000

Total execution time (in seconds): 0.086530

Mandelbrot set: Computed
Histogram for Mandelbrot set: Computed
2048

Computation of the Mandelbrot set with:
    center = (0, 0) 
    size = 2
    maximum iterations = 10000

Total execution time (in seconds): 0.087279

Mandelbrot set: Computed
Histogram for Mandelbrot set: Computed
4096

Computation of the Mandelbrot set with:
    center = (0, 0) 
    size = 2
    maximum iterations = 10000

Total execution time (in seconds): 0.081736

Mandelbrot set: Computed
Histogram for Mandelbrot set: Computed
8192

Computation of the Mandelbrot set with:
    center = (0, 0) 
    size = 2
    maximum iterations = 10000

Total execution time (in seconds): 0.079485

Mandelbrot set: Computed
Histogram for Mandelbrot set: Computed
16384

Computation of the Mandelbrot set with:
    center = (0, 0) 
    size = 2
    maximum iterations = 10000

Total execution time (in seconds): 0.081457

Mandelbrot set: Computed
Histogram for Mandelbrot set: Computed
32768

Computation of the Mandelbrot set with:
    center = (0, 0) 
    size = 2
    maximum iterations = 10000

Total execution time (in seconds): 0.084295

Mandelbrot set: Computed
Histogram for Mandelbrot set: Computed
