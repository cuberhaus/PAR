1

Computation of the Mandelbrot set with:
    center = (0, 0) 
    size = 2
    maximum iterations = 10000

Total execution time (in seconds): 2.871713

Mandelbrot set: Computed
Histogram for Mandelbrot set: Computed
2

Computation of the Mandelbrot set with:
    center = (0, 0) 
    size = 2
    maximum iterations = 10000

Total execution time (in seconds): 2.101162

Mandelbrot set: Computed
Histogram for Mandelbrot set: Computed
4

Computation of the Mandelbrot set with:
    center = (0, 0) 
    size = 2
    maximum iterations = 10000

Total execution time (in seconds): 1.876674

Mandelbrot set: Computed
Histogram for Mandelbrot set: Computed
8

Computation of the Mandelbrot set with:
    center = (0, 0) 
    size = 2
    maximum iterations = 10000

Total execution time (in seconds): 1.329546

Mandelbrot set: Computed
Histogram for Mandelbrot set: Computed
16

Computation of the Mandelbrot set with:
    center = (0, 0) 
    size = 2
    maximum iterations = 10000

Total execution time (in seconds): 0.780821

Mandelbrot set: Computed
Histogram for Mandelbrot set: Computed
32

Computation of the Mandelbrot set with:
    center = (0, 0) 
    size = 2
    maximum iterations = 10000

Total execution time (in seconds): 0.466791

Mandelbrot set: Computed
Histogram for Mandelbrot set: Computed
64

Computation of the Mandelbrot set with:
    center = (0, 0) 
    size = 2
    maximum iterations = 10000

Total execution time (in seconds): 0.370507

Mandelbrot set: Computed
Histogram for Mandelbrot set: Computed
128

Computation of the Mandelbrot set with:
    center = (0, 0) 
    size = 2
    maximum iterations = 10000

Total execution time (in seconds): 0.435243

Mandelbrot set: Computed
Histogram for Mandelbrot set: Computed
256

Computation of the Mandelbrot set with:
    center = (0, 0) 
    size = 2
    maximum iterations = 10000

Total execution time (in seconds): 0.817556

Mandelbrot set: Computed
Histogram for Mandelbrot set: Computed
512

Computation of the Mandelbrot set with:
    center = (0, 0) 
    size = 2
    maximum iterations = 10000

Total execution time (in seconds): 0.714996

Mandelbrot set: Computed
Histogram for Mandelbrot set: Computed
1024

Computation of the Mandelbrot set with:
    center = (0, 0) 
    size = 2
    maximum iterations = 10000

Total execution time (in seconds): 0.452920

Mandelbrot set: Computed
Histogram for Mandelbrot set: Computed
2048

Computation of the Mandelbrot set with:
    center = (0, 0) 
    size = 2
    maximum iterations = 10000

Total execution time (in seconds): 0.450755

Mandelbrot set: Computed
Histogram for Mandelbrot set: Computed
4096

Computation of the Mandelbrot set with:
    center = (0, 0) 
    size = 2
    maximum iterations = 10000

Total execution time (in seconds): 0.447406

Mandelbrot set: Computed
Histogram for Mandelbrot set: Computed
8192

Computation of the Mandelbrot set with:
    center = (0, 0) 
    size = 2
    maximum iterations = 10000

Total execution time (in seconds): 0.451692

Mandelbrot set: Computed
Histogram for Mandelbrot set: Computed
16384

Computation of the Mandelbrot set with:
    center = (0, 0) 
    size = 2
    maximum iterations = 10000

Total execution time (in seconds): 0.455062

Mandelbrot set: Computed
Histogram for Mandelbrot set: Computed
32768

Computation of the Mandelbrot set with:
    center = (0, 0) 
    size = 2
    maximum iterations = 10000

Total execution time (in seconds): 0.452044

Mandelbrot set: Computed
Histogram for Mandelbrot set: Computed
